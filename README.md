# RainbruRPG

Here is an open source 3D MMORPG project.

## Dependencies

If you're using a Debian-based GNU/linux distribution :

	sudo apt-get --yes install libogre-1.9-dev libois-dev libboost-program-options-dev libboost-system-dev \
	  libboost-filesystem-dev libboost-graph-dev libboost-test-dev libboost-regex-dev  \
      libenet-dev guile-2.0-dev libgit2-dev libssh-dev clang libgtest-dev google-mock gettext 

on arch-based ones such as manjaro :

	sudo pacman -S ogre ois

CEGUI can't be find in official repository, you must install it from 
[AUR](https://aur.archlinux.org/packages/cegui).
  
## Building CEGUI

These can't be found anymore : libcegui-mk2-dev

## Build and installation

You should be able to build all components :

	git submodule init
	git submodule update
	mkdir build
	cd build
	cmake ..
	make
	make check
	./rainbrurpg-client

Using some GNU/Linux distribution, if `cmake` can't find *gtest*, 
maybe you have to manually build it :

	cd /usr/src/gtest/
	#mkdir build
	#cd build
	#cmake ..
	#make
	#make install

This should install *libgtest.a* in */usr/local/lib/*, and `cmake` now should 
find it.

## Client key bindings

The following keys work in main menu :

Key         | Action
------------|------------------
ESC         | Exit the game
F11         | Create a rainbrurpg-<timestamp>.png screenshot
Alt + Enter | Switch fullscreen
Alt + +     | Next greater screen resolution
Alt + -     | Next lesser screen resolution

## API documentation

API documentation is generated using `doxygen`. Before building it you should 
install :

	apt-get install doxygen graphviz
	
then run :
	
	doxygen

## Development status

The project is a work-in-progress. The game itself isn't playable.

# Get Involved

If you have an idea for a new feature or find a bug in RainbruRPG, _please_ 
file an issue for it!. 
Any and all help here is appreciated, even if that just means trying out 
the client for a day.

If you'd like to tackle something, but don't know where to start, _please_ 
let me know! I'd love to help you get involved, so feel free to ask and I'll 
do my best to get you up and running.

## The Basics

If you would like to contribute to RainbruRPG's development, just:

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Owning an issue

If you have a specific issue
that you'd like to tackle, be sure to add a comment saying you're working on 
it so that everyone is aware! 

Also, "ownership" is _not_ binding. It's just a way of saying "hey, I think 
I can work on this!". If you get stuck or need help moving forward, feel free 
to ask for help.

Most importantly, **don't feel bad if you bite off more than you can chew**. 
Issues can easily end up being far more complex than they appear at the start, 
especially on a project of this size. But don't give up! It's always hard to 
get started on an existing project, but I want to help and make it as easy as 
possible wherever I can!

# License

	Copyright (C) 2006-2018 Jérôme Pasquier

	rainbrurpg is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	rainbrurpg is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.

# Links

[http://dailycommit.blogspot.com/](http://dailycommit.blogspot.com/): 
development blog.
