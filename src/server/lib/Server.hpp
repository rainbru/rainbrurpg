/* 
 * server - The RainbruRPG's server binary.
 *
 * Copyright (C) 2011-2012, 2014-2018 Jérôme Pasquier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _SERVER_HPP_
#define _SERVER_HPP_

#include <string>

#include <boost/thread.hpp>

/** The prefix used to create $HOME subdirectory
  *
  * Will be used to call RLGL::Systeùm instances.
  *
  */
#define GAME_PREFIX ".rainbrurpg"

// Forward declarations
namespace RLGL
{
  class System;
  class MapGenerator;
}
// End of forward declarations

typedef enum {
  STT_STANDALONE,
  STT_EMBEDDED,
}ServerType_t;

/** The main server class 
  *
  *
  * Defines a standalone or an umbeddable server according to the way
  * we launch it (from localtest or from the server binary).
  *
  * This class is also the one that deals with RLGL to load or generate
  * the world geometry.
  *
  */
class Server
{
public:
  Server(ServerType_t);
  ~Server();

  void load(const std::string&);
  void generate(const std::string&, const std::string&);

  RLGL::MapGenerator* getGenerator();

  void joinThread();
  
protected:
  void rlgl_generate();


private:
  std::string world_name;
  std::string seed;

  RLGL::System*    rSys;
  RLGL::MapGenerator* mGenerator;
  boost::thread mThread;
};

#endif // _SERVER_HPP_
