/* 
 * server - The RainbruRPG's server binary.
 *
 * Copyright (C) 2011-2012, 2014-2018 Jérôme Pasquier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Server.hpp"

#include "Logger.hpp"

#include <RlglSystem.hpp>
#include <MapGenerator.hpp>

static Rpg::Logger static_logger("server", Rpg::LT_BOTH);

Server::Server(ServerType_t):
  rSys(NULL),
  mGenerator(NULL)
{
  rSys = new RLGL::System(GAME_PREFIX);
  mGenerator = rSys->getGenerator();
  //  mGenerator->subscribe(lb);

}

Server::~Server()
{
  delete rSys;
  /* The following causes a double-free corruption error when leaving the game
   * after the LocalTest map generation thing. Don't really know why but it
   * is something maybe due to the use of a separated thread.
   */
  //  delete mGenerator;
}

/** Load an existing world
  * 
  * \param name The name of the world to be loaded
  *
  */
void
Server::load(const std::string& name)
{
  LOGI("Loading existing world from the '" << name << "' directory");
  
}

/** Generate an new world
  * 
  * \param name The name of the world to be generated
  * \param seed The generation seed
  *
  */
void
Server::generate(const std::string& name, const std::string& seed)
{
  world_name = name;
  this->seed = seed;
  mGenerator->setMapName(name);
  LOGI("Generating new world named" << name << "with seed" << seed);

  mThread = boost::thread( &Server::rlgl_generate, this );
}

void
Server::rlgl_generate()
{
  mGenerator->generate();
  
}

RLGL::MapGenerator*
Server::getGenerator()
{
  return mGenerator;
}

/** Join the map genartion thread
  *
  * This function shoudl be called from the main thread (i.e. from GameEngine).
  *
  */
void
Server::joinThread()
{
  mThread.join();
}
