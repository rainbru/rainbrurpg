/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Server.hpp"

#include "gtest/gtest.h"

#include "Seed.hpp"
#include "MapGenerator.hpp"

using namespace std;

TEST( Server, constructor_embedded )
{
  Server s(STT_EMBEDDED);
}

TEST( Server, constructor_standalone )
{
  Server s(STT_STANDALONE);
}

TEST( Server, get_generator )
{
  Server s(STT_EMBEDDED);
  EXPECT_TRUE(s.getGenerator() != NULL);
}

/* Simply test that calling Server::generate actually generates a world
 *
 */
TEST( Server, generate )
{
  /*
  Server s(STT_EMBEDDED);

  RLGL::Seed se;
  se.randomize();

  s.generate("unit_tests", se.to_s());
  string mapdir = s.getGenerator()->getMapDirectory();
  ASSERT_EQ(mapdir, ""); // Shouldn't be rmpty
  */
}

// Mainly test that joinThread doesn't cause a segfault
TEST( Server, join )
{
  /*  Server s(STT_EMBEDDED);

  RLGL::Seed se;
  se.randomize();

  s.generate("unit_tests", se.to_s());
  //  s.joinThread();  // Actually cause a segfault
  */
}
