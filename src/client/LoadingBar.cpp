/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "LoadingBar.hpp"

#include <CEGUI/WindowManager.h>

#include <CEGUI/Window.h>
#include <CEGUI/widgets/ProgressBar.h>

#include "Logger.hpp"

static Rpg::Logger static_logger("engine", Rpg::LT_BOTH);

LoadingBar::LoadingBar(const string& vTitle):
  title(vTitle),
  mWmgr(NULL),
  stepLabel(NULL),
  progressbar(NULL),
  mLayoutWindow(NULL),
  stepSize(0.0f),
  currentStep(0)
{
  // Do not instanciate anything Ogre/CEGUI related here

  
}

void
LoadingBar::init()
{
  mWmgr = CEGUI::WindowManager::getSingletonPtr();

  mLayoutWindow = mWmgr->loadLayoutFromFile("loading_bar.layout");
  mLayoutWindow->setVisible(true);

  CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()
    ->addChild(mLayoutWindow);

  CEGUI::Window* titleLabel = mLayoutWindow->getChild("titleLabel");
  titleLabel->setProperty("Text", title);
  
  stepLabel = mLayoutWindow->getChild("stepLabel");
  if (stepNames.size() > 0)
    stepLabel->setProperty("Text", stepNames[0]);

  progressbar = static_cast<CEGUI::ProgressBar*>
    (mLayoutWindow->getChild("ProgressBar"));
  progressbar->setProgress(0.0f);
}


LoadingBar::~LoadingBar()
{

}

size_t
LoadingBar::getStepNumber()
{
  return stepNames.size();
}


void
LoadingBar::addStep(const string& name)
{
  stepNames.push_back(name);
  adjustProgress();
}

void
LoadingBar::adjustProgress()
{
  stepSize = 1.0f / getStepNumber();

  if (progressbar)
    progressbar->setStepSize(stepSize);

  
  LOGI("Stepsize = " << stepSize);

}


float
LoadingBar::getStepSize()
{
  return stepSize;
}

void
LoadingBar::step()
{
  auto limit = stepNames.size();
  currentStep++;

  // Only if label present, UI update
  if (stepLabel)
    {
      
      // If we test using  <= here, we may have a segfault
      if (currentStep < limit)
	stepLabel->setProperty("Text", stepNames[currentStep]);
    }
  
  if (progressbar)
    progressbar->step();

}

size_t
LoadingBar::getCurrentStep()
{
  return currentStep;
}

void
LoadingBar::setTitle(const std::string& t)
{
  title = t;
  CEGUI::Window* titleLabel = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild("loading/titleLabel");
  titleLabel->setProperty("Text", t);
}

void
LoadingBar::loadingDone()
{
    LOGI("Loading's done");
  
}

void
LoadingBar::show()
{
  mLayoutWindow->setVisible(true);
}

void
LoadingBar::hide()
{
  mLayoutWindow->setVisible(false);
}
