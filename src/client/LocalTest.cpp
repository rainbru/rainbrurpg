/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <CEGUI/Window.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/CEGUI.h>

//#include <time.h>       // Uses time 
//#include <cstdlib>      // Uses srand, rand

#include "LocalTest.hpp"

#include "GameEngine.hpp"
#include "WaitingCircle.hpp"
#include "StateSaver.hpp"

#include "Server.hpp"
#include "Logger.hpp"
#include "Seed.hpp"

#include "LoadingBar.hpp"

#include <RlglSystem.hpp>
#include <MapGenerator.hpp>

using namespace std;
using namespace CEGUI;
using namespace RLGL;

static Rpg::Logger static_logger("state", Rpg::LT_BOTH);

LocalTest::LocalTest():
  GameState("LocalTest"),
  mGameEngine(NULL),
  mMenuWindow(NULL),
  lbWorlds(NULL),
  mTabControl(NULL),
  mWaiting(NULL),
  mServer(NULL),
  mLoadingBar(NULL),
  toGame(false)
{
 
}

LocalTest::~LocalTest()
{

}
  
void
LocalTest::enter(GameEngine* ge)
{
  // Keep a pointer to GameEngine to be able to go back to MainMenu
  mGameEngine = ge;

  try {
    mMenuWindow = loadLayout("local_test.layout", "LocalTestWin");
  }
  catch(CEGUI::UnknownObjectException& e)
    {
      LOGE("Error, loading LocalTest layout");
    }
  
  WindowManager& winMgr = WindowManager::getSingleton();
  mTabControl = static_cast<TabControl*>(mMenuWindow->getChild("TabControl"));
  mTabControl->setTabHeight(UDim(0.15f, 0.0f)); // Make the tab buttons a little bigger
  Window* tabPage = mMenuWindow->getChild("TabPane1");
  tabPage->setText("  New world  ");
  mTabControl->addTab(tabPage);
  tabPage->setProperty("Size", "{{1,0},{1,0}}");
  tabPage->setProperty("Position", "{{0,0},{0,0}}");

  Window* tabPage2 = mMenuWindow->getChild("TabPane2");
  tabPage2->setText("  Existing world  ");
  mTabControl->addTab(tabPage2);
  tabPage2->setProperty("Size", "{{1,0},{1,0}}");
  tabPage2->setProperty("Position", "{{0,0},{0,0}}");

  randomSeed();

  // Add some existing worlds
  lbWorlds = static_cast<Listbox*>
    (mMenuWindow->getChild("TabControl/TabPane2/lbExisting"));
  lbWorlds->setMultiselectEnabled(false);
  lbWorlds->setSortingEnabled(true);

  // Handle events
  addEvent("LocalTestWin/TabControl/TabPane1/btnRandom",
	   CEGUI::PushButton::EventClicked,
	   CEGUI::Event::Subscriber(&LocalTest::randomSeed, this));
  addEvent("LocalTestWin/btnBack", CEGUI::PushButton::EventClicked,
	   CEGUI::Event::Subscriber(&LocalTest::onBack, this));
  addEvent("LocalTestWin/btnPlay", CEGUI::PushButton::EventClicked,
	   CEGUI::Event::Subscriber(&LocalTest::onPlay, this));
  addEvent("LocalTestWin/TabControl", CEGUI::TabControl::EventSelectionChanged,
  	   CEGUI::Event::Subscriber(&LocalTest::onTabChange, this));
  addEvent("LocalTestWin/TabControl/TabPane2/lbExisting",
	   CEGUI::TabControl::EventSelectionChanged,
	   CEGUI::Event::Subscriber(&LocalTest::onSelectionChange, this));
  
  LOGI("LocalTest signals successfully registered");

  mWaiting = new WaitingCircle("Parsing local worlds...", 0.6f);
  mWaiting->hide();
  
  // Handle LocalWorlds pubsub
  RLGL::System* rs = new RLGL::System(GAME_PREFIX);
  rs->subscribe(this);     // Mark this instance as a listener
  rs->parseLocalWorlds(".rainbrurpg");
  
  // For test purpose
  /*  addWorld("aze");
  addWorld("aze2");
  addWorld("aze3");
  */
}


void
LocalTest::exit(GameEngine*)
{
  //  CEGUI::WindowManager::getSingleton().destroyWindow(mMenuWindow);
  destroyRootWindow();
  delete mWaiting;

}

/** Get a new random seed and set it to the GUI text box.
  *
  *
  */
void
LocalTest::randomSeed()
{
  RLGL::Seed s;
  s.randomize();
  setSeed(s.to_s());
  check();
}

/** Go back to the main menu */
bool
LocalTest::onBack(const CEGUI::EventArgs&)
{
  mGameEngine->toMainMenu();
  return true;
}

bool
LocalTest::keyPressed( const OIS::KeyEvent& )
{
  check();
  return false;
}

bool
LocalTest::onTabChange(const CEGUI::EventArgs&)
{
  check();
  return true;
}


/** Check for the playability of the form
  *
  * This function shouldn't change the existing worlds list box selection
  * to avoid an infinite loop.
  *
  */
void
LocalTest::check()
{
  // Must enable play button ?
  bool play = false;
  
  // Get the active Tabcontrol
  TabControl* tc=static_cast<TabControl*>(mMenuWindow->getChild("TabControl"));
  int sel = tc->getSelectedTabIndex();

  if (sel ==0 ) /* New world tab */
    {
      // Must test if name and seed aren't empty
      bool s2 = !mMenuWindow->getChild("TabControl/TabPane1/ebName")
	->getText().empty();
      play = !getSeed().empty() && s2;
    }
  else /* Existing world tab */
    {
      // Must test if a world is selected
      Listbox* lb = static_cast<Listbox*>
	(mMenuWindow->getChild("TabControl/TabPane2/lbExisting"));
      
      play = lb->getSelectedCount() != 0;
    }

  // Change the Play button state
  Window* btnPlay = mMenuWindow->getChild("btnPlay");
  if (play)
    btnPlay->setProperty("Disabled", "False");
  else
    btnPlay->setProperty("Disabled", "True");
    
}

/** Event handler fired when the existing worlds list box selection changed
  *
  */
bool
LocalTest::onSelectionChange(const CEGUI::EventArgs&)
{
  check();
  return true;
}

void
LocalTest::addWorld(const string& worldName)
{
  // lbWorlds
  ListboxTextItem* lbi = new ListboxTextItem(worldName, 1);
  lbi->setSelectionColours(CEGUI::Colour(0.0f, 0.0f, 0.8f));
  lbi->setTextColours(CEGUI::Colour(0xFFFFFFFF));
  lbi->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
  lbWorlds->addItem(lbi);
}

void
LocalTest::drawOverlay()
{
  mWaiting->draw();
}

void
LocalTest::update(float elapsed)
{
  mWaiting->update(elapsed);

  // Deferred change to Game state
  if (toGame)
    {
      mServer->joinThread();
      mServer->getGenerator()->unsubscribe(this);
      mGameEngine->toGame(mServer, &mNewMapSettings);
    }

}

void
LocalTest::save(StateSaver* st)
{
  // Scalars
  st->set<string>("seed", getSeed());
  st->set<string>("name", getName());

  // Complex types
  st->save("localtest",      mMenuWindow);
  st->save("waiting-circle", mWaiting);
  st->save("tab",            mTabControl);
  st->save("worlds",         lbWorlds);
  
}


void
LocalTest::restore(StateSaver* st)
{
  // Scalars
  setSeed(st->get<string>("seed"));
  setName(st->get<string>("name"));
    
  // Complex types
  st->restore("localtest",      mMenuWindow);
  st->restore("waiting-circle", mWaiting);
  st->restore("tab",            mTabControl);
  st->restore("worlds",         lbWorlds);

}

void
LocalTest::parsingStarted(int nbWorlds)
{
  LOGI("Local worlds parsing starts for" << nbWorlds << "worlds");
  if (nbWorlds > 20)
    mWaiting->show();
}

void
LocalTest::parsingFinished()
{
  mWaiting->hide();
}

void
LocalTest::gotWorld(const std::string& worldName)
{
  addWorld(worldName);
}

string
LocalTest::getSeed() const
{
  Window* teSeed = mMenuWindow->getChild("TabControl/TabPane1/ebSeed");
  std::string str = teSeed->getText().c_str();
  return str;
}

void
LocalTest::setSeed(const string& seed)
{
  Window* teSeed = mMenuWindow->getChild("TabControl/TabPane1/ebSeed");
  teSeed->setText(seed);
}

string
LocalTest::getName() const
{
  Window* teSeed = mMenuWindow->getChild("TabControl/TabPane1/ebName");
  std::string str = teSeed->getText().c_str();
  return str;
}

void
LocalTest::setName(const string& name)
{
  Window* teSeed = mMenuWindow->getChild("TabControl/TabPane1/ebName");
  teSeed->setText(name);
}

bool
LocalTest::onPlay(const CEGUI::EventArgs&)
{
  TabControl* tc=static_cast<TabControl*>(mMenuWindow->getChild("TabControl"));
  int sel = tc->getSelectedTabIndex();

  mServer = new Server(STT_EMBEDDED);
  mGameEngine->setServer(mServer);

  // Disable play button to avoid creating loadingbar twice
  Window* btnPlay = mMenuWindow->getChild("btnPlay");
  btnPlay->setProperty("Disabled", "True");

  
  if (sel ==0 ) /* New world tab */
    {
      mLoadingBar = new LoadingBar();
      mGameEngine->setLoadingBar(mLoadingBar);
      mLoadingBar->init();

      mNewMapSettings.new_map = true;
      
      string n =getName();
      string s =getSeed();
      mLoadingBar->setTitle("Generating new world");
      mServer->getGenerator()->setSeed(s);
      mServer->getGenerator()->subscribe(mLoadingBar);
      // Register ourself as a subscriber, for the loadingDone callback
      mServer->getGenerator()->subscribe(this);
      //      mGameEngine->toMainMenu();
      mMenuWindow->hide();
      mServer->generate(n, s);
    }
  else /* Existing world tab */
    {
    
      mMenuWindow->hide();
      // Must get text of selected world
      string n = lbWorlds->getFirstSelectedItem()->getText().c_str();
      mNewMapSettings.new_map = false;
      mNewMapSettings.map_directory = n;

      loadingDone();
    }
  return true;
}

/** Override from LoadingBarListener
  *
  * Map generation's done, we can change current gamestate
  *
  */
void LocalTest::loadingDone()
{
  /* Setting the map's name if the map is new */
  if (mNewMapSettings.new_map){
    mNewMapSettings.map_directory = mServer->getGenerator()->getMapDirectory();
  }
  
  if (mLoadingBar)
    mLoadingBar->hide();

  toGame = true;
  
}
