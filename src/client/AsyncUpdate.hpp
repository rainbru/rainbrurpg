/*
 * Copyright 2011-2018 Jérôme Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _ASYNC_UPDATE_
#define _ASYNC_UPDATE_


/** A multi-purpose async update class
  *
  * When a non-modal CEGUI dialog intend to make an update on another layout :
  * 1. the main class must inherit AsyncUpdate
  * 2. override/reimplement update
  * 3. The dialog takes a AsyncUpdate* as constructor parameter
  * 4. Then call parent->asyncUpdate when needed
  *
  */
class AsyncUpdate
{
public:
  virtual void asyncUpdate()=0;
};

#endif // !_ASYNC_UPDATE_
