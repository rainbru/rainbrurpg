/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "RsaKey.hpp"

#include <iostream>
#include <sstream>

#include <stdlib.h>      /* srand */
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>  /* BIGNUM */
#include <openssl/bio.h> /* BIO* */
#include <openssl/pem.h>
#include <openssl/err.h> /* Uses ERR_error_string() */
#include <openssl/rand.h>

#include <Logger.hpp>

static Rpg::Logger static_logger("rsa", Rpg::LT_BOTH);

/** Named constructor
  *
  * This won't generate or write keys to disk.
  *
  * \param bn The basename for the allocated key
  *
  */
RsaKey::RsaKey(const string& bn):
  privateKeyName(bn),
  publicKeyName(bn + ".pub"),
  rsaPrivateKey(NULL),
  placeholder_pwd("placeholder_pwd")
{

  if(SSL_library_init())
  {
      SSL_load_error_strings();
      OpenSSL_add_all_algorithms();
      RAND_load_file("/dev/urandom", 1024);
    }

}

/** Free the memory used by this object
  *
  */
RsaKey::~RsaKey()
{
  RSA_free(rsaPrivateKey);
}

/** Write the key with the given passphrase
  *
  * \param passphrase The password used to write private key
  *
  */
int
RsaKey::write(string& passphrase)
{
  /* initialize random seed: */
  srand (time(NULL));
  if (passphrase.empty())
    return (writePublicKey() + writePrivateKey(placeholder_pwd));
  else
    return (writePublicKey() + writePrivateKey(passphrase));
}


/** Read both public and private keys 
  *
  * \return The RSA struct or NULL if an error occured
  */
RSA*
RsaKey::read(string& passphrase)
{
  int p = readPublicKey();
  if (p != 0)
    return NULL;
  
  return readPrivateKey(passphrase);
}

int
RsaKey::writePublicKey()
{
  RSA* rsa = RSA_new();
  BIGNUM* exponent = BN_new();
  BN_set_word(exponent, 17);
  
  int g = RSA_generate_key_ex(rsa,
			      1024, /* Key size in bits */
			      exponent, /* exponent */
			      NULL  /* callabck */);
  // callback signature 'void (*callback)(int,int,void *)'

  if (rsa == NULL)
    {
      LOGE("RSA key geneartion failed");
      return 1;
    }

  // Writing public key to BIO file
  LOGI("Writting public key to " << publicKeyName);
  BIO* bio_public = BIO_new_file(publicKeyName.c_str(), "w");
  if (! PEM_write_bio_RSAPublicKey(bio_public, rsa))    
    {
      LOGE("Can't write public key to file");
      return 1;
    }

  BIO_flush(bio_public);
  BIO_set_close(bio_public, BIO_CLOSE); 

  RSA_free(rsa);
  BN_free(exponent);
  return 0;
  
}

int
RsaKey::writePrivateKey(string& passphrase)
{
  BIGNUM          *bne = NULL;
  int             bits = 2048;
  unsigned long   e = RSA_F4;

  RSA* rsa = RSA_new();


  bne = BN_new();
  BN_set_word(bne,e);
  int ret = RSA_generate_key_ex(rsa, bits, bne, NULL);
  assert(ret == 1 && "Can't generate key_ex");

  // Writing private key to BIO file
  BIO* bio_private = BIO_new_file(privateKeyName.c_str(), "w+");
  
  /*
    For the PEM write routines if the kstr parameter is not NULL then klen 
    bytes at kstr are used as the passphrase and cb is ignored.

    Unused params are used when using a callback or prompt to get the 
    passphrase.

    std::string to void* trick from https://stackoverflow.com/a/14553723
    
  */
  int wret = PEM_write_bio_RSAPrivateKey(bio_private, // BIO *bp 
					 rsa,         // RSA *x 
					 EVP_des_ede3_cbc(),
					 NULL, 0, 0, /* Unused cb, etc ... */
					 &passphrase[0]);
  
  
  if (!wret)
    {
      LOGE("Can't write private key to file '" << privateKeyName << "'");
      return 1;
    }
  
  // If omitted, we can't read BIOs after, getting a "error:0906D06C:PEM
  // routines:PEM_read_bio:no start line" error
  BIO_flush(bio_private);
  
  // Close the underlying FILE objects
  BIO_set_close(bio_private, BIO_CLOSE);

  // Free RSA
  RSA_free(rsa);


  // Chack that the file was correctly written
  std::ifstream infile(privateKeyName);
  if (!infile.good())
     {
       string err = "File doesn't exist!";
       LOGE("Can't write private key" << privateKeyName << " : " << err );
      cout << "Can't write private key" << privateKeyName << " : " <<
	err << endl;

       return 1;
     }
  else
    {
       LOGI("Just written" << privateKeyName  );
      
    }

  return 0;
}

int
RsaKey::readPublicKey()
{
  RSA* rsa = NULL; //RSA_new();
  // First read public key
  BIO* bio_public = BIO_new_file(publicKeyName.c_str(), "r");
  if (!PEM_read_bio_RSAPublicKey(bio_public, NULL, NULL, NULL))
    {
      LOGE("Can't read key from file. PEM_read_bio_RSA_PUBKEY FAILED:" <<
	   ERR_error_string( ERR_get_error(), NULL ) );
      return 1;
    }

  RSA_free(rsa);
  return 0;
}

RSA*
RsaKey::readPrivateKey(string& passphrase)
{
  // Test if file exists
   std::ifstream infile(privateKeyName);
   if (!infile.good())
     {
       string err = "File doesn't exist!";
       LOGE("Can't read private key" << privateKeyName << " : " << err );
      cout << "Can't read private key" << privateKeyName << " : " <<
	err << endl;

       return NULL;
     }

    
    // Read the key
  rsaPrivateKey = RSA_new();
  BIO* bio_private = BIO_new_file(privateKeyName.c_str(), "r");
  if (!PEM_read_bio_RSAPrivateKey(bio_private, &rsaPrivateKey, NULL, &passphrase[0]))
    {
      LOGE("Can't read private key" << privateKeyName << " : " <<
	   ERR_error_string( ERR_get_error(), NULL ));
      cout << "Can't read private key" << privateKeyName << " : " <<
	ERR_error_string( ERR_get_error(), NULL ) << endl;
      return NULL;
    }
  return rsaPrivateKey;
}

const string&
RsaKey::getPrivateKeyName() const
{
  return privateKeyName;
}

const string&
RsaKey::getPublicKeyName() const
{
  return publicKeyName;
}

/** Return true if the actual key needs a password
  * 
  */
bool
RsaKey::hasPassword()
{
  // Needed as a reference
  string empty = "";
  RSA* r = readPrivateKey(placeholder_pwd);
  cout << "in hasPassword(): r = " << r << endl;;
    
  return ( r == NULL);
}
