/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "KeyList.hpp"
#include <gtest/gtest.h>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

const string KEY_FILENAME = "keylist-test";

/** A simple KeyList subclass used to make private members/methods public
  *
  */
class _KeyList : public KeyList
{
public:
  /** Public constructor */
  _KeyList()
  {
 
    keylist = KEY_FILENAME;
    keydir = "./";

  }
  
};

TEST( KeyList, add )
{
  _KeyList y;
  auto s = y.mapSize();
  y.add("tobe-sanitized.username");
  ASSERT_EQ(y.mapSize(), ++s);
}

// Check if the test file doesn't exist
TEST( KeyList, doesnt_exist )
{
  /*
  fs::path p("./" + KEY_FILENAME);
  ASSERT_FALSE(exists(p));
  */
}

// Test that save effectively create underlying file
TEST( KeyList, save_create_file )
{
  _KeyList y;
  y.save();
  fs::path p("./" + KEY_FILENAME);
  ASSERT_TRUE(exists(p));
  y.remove_keylist();
}

// Test that we can remove the keylist file
TEST( KeyList, remove_keylist )
{
  _KeyList y;
  y.save();
  fs::path p("./" + KEY_FILENAME);
  y.remove_keylist();
  ASSERT_FALSE(exists(p));
}

TEST( KeyList, load_saved_file )
{
  _KeyList y;
  y.add("key1");
  y.add("this.incredibly-long!name*should-be-sanitized");
  y.save();

  _KeyList y2;
  y2.load();
  ASSERT_EQ(y2.mapSize(), y.mapSize());
}

TEST( KeyList, check_key )
{
  _KeyList y;
  ASSERT_TRUE(y.checkKey("aze"));
  ASSERT_FALSE(y.checkKey("az,e"));
}

TEST( KeyList, get_keys )
{
  _KeyList y;
  list<string> l = y.getKeys();
  ASSERT_EQ(l.size(), y.mapSize());

}

TEST( KeyList, get_keys_2 )
{
  _KeyList y;
  y.add("key1");
  y.add("this.incredibly-long!name*should-be-sanitized");
  list<string> l = y.getKeys();
  ASSERT_EQ(l.size(), y.mapSize());

}

// Saved and returned keys shouldn't have double quotes as first/last char
TEST( KeyList, get_key_noquotes )
{
  _KeyList y;
  y.add("key1");
  y.save();

  _KeyList y2;
  y2.load();
  list<string> l = y2.getKeys();

  char c1 = l.front()[0];
  ASSERT_FALSE(c1 == '\"');

  char c2 = l.front().back();
  ASSERT_FALSE(c2 == '\"');
  
}
