/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "CeguiWindowList.hpp"

#include <CEGUI/Window.h>
#include <CEGUI/WindowManager.h>
#include <CEGUI/CEGUI.h>

#include <iostream>

using namespace std;
using namespace CEGUI;

#define TREE  "└── "
#define EMPTY "    "

void
print_indent(int indent)
{
  if (indent > 0)
    {
      for (int i = 0; i < indent - 1;  i++)
	cout << EMPTY;
	
      cout << TREE;
    }
}

void
print_childs(Window* wnd, int indent)
{
  size_t index = 0;
  while (index < wnd->getChildCount())
    {
      print_indent(indent);
      CEGUI::Window* child = wnd->getChildAtIdx(index);
      cout << child->getName() << endl;
      print_childs(child, indent + 1);
      
      ++index;
    } 

}

/** A simple helper function to print registered CEGUI windows
  *
  */
void
print_window_list()
{
  int indent = 0;
  
  cout << "** Printing window list" << endl;
    WindowManager& winMgr = WindowManager::getSingleton();
    auto it = winMgr.getIterator();

    for(; !it.isAtEnd(); it++) {
      cout << it.getCurrentValue()->getName() << endl;
      print_childs(it.getCurrentValue(), indent+1);
    }
}
