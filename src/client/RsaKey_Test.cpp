/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "RsaKey.hpp"
#include <gtest/gtest.h>

string passphrase = "testing_key_generation";
string emptypwd = "";

TEST( RsaKey, privatename )
{
  string bn = "bio-test";
  RsaKey rk(bn);
  ASSERT_EQ(rk.getPrivateKeyName(), bn);
}

TEST( RsaKey, publicname )
{
  string bn = "bio-test";
  string bnp = "bio-test.pub";
  RsaKey rk(bn);
  ASSERT_EQ(rk.getPublicKeyName(), bnp);
}

TEST( RsaKey, write )
{
  RsaKey rk("bio-test");
  ASSERT_EQ(rk.write(passphrase), 0);
}

TEST( RsaKey, read_wrong_pp )
{
  string pw = "wrong";
  RsaKey rk("bio-test");
  RSA* r = rk.read(pw);
  ASSERT_FALSE(r);
}

// If right passphrase, should return a valid RSA pointer
TEST( RsaKey, read )
{
  RsaKey rk("bio-test-read");
  rk.write(passphrase);
  RSA* r = rk.read(passphrase);
  ASSERT_TRUE(r != NULL);
}

// If wrong passphrase, return NULL
TEST( RsaKey, read_null )
{
  RsaKey rk("bio-test-read");
  rk.write(passphrase);
  RSA* r = rk.read(passphrase);
  ASSERT_TRUE(r != NULL);
}

// Test if a Rsa key has a password
TEST( RsaKey, has_password )
{
  RsaKey rk2("bio-test2");
  rk2.write(passphrase);
  ASSERT_TRUE( rk2.hasPassword());
}

// If a key was created with no password, hasPassword() should return false
TEST( RsaKey, has_password_no )
{
  RsaKey rk("bio-test-nopwd");
  rk.write(emptypwd);
  ASSERT_FALSE( rk.hasPassword());
}
