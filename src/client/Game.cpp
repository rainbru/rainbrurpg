/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Game.hpp"

#include "Server.hpp"
#include "Logger.hpp"
#include "WaitingCircle.hpp"
#include "GameEngine.hpp"

#include <Ogre.h>

static Rpg::Logger static_logger("game", Rpg::LT_BOTH);

Game::Game():
  GameState("Game"),
  mServer(NULL),
  mMapSettings(NULL),
  mWaiting(NULL),
  mCamera(NULL),
  mGameEngine(NULL),
  mRotate(.13),
  mMove(250),
  mDirection(Ogre::Vector3::ZERO),
  mMoving(false)
{

}

Game::~Game()
{
  // Do not destroy mServer here
}
  
void
Game::enter(GameEngine* ge)
{
  mGameEngine=ge;

  if (!mServer)
    {
      LOGE("Enetering a NULL server may fail!");
    }
  
  if (!mMapSettings)
    {
      LOGE("Entering a NULL NewMapSettings object will fail!");
    }
  else
    {
      LOGI("Entering Gamestate for map " << mMapSettings->map_directory);
    }

  
  mCamera = Ogre::Root::getSingleton().getSceneManager("terrain")
    ->getCamera("PlayerCam");
  mCamera->setPosition(3.0, 3.0, 3.0);
  mCamera->setProjectionType( Ogre::ProjectionType::PT_PERSPECTIVE);

  mWaiting = new WaitingCircle("", 0.6f);
  mWaiting->show();

  mServer->load(mMapSettings->map_directory);
  testScene();
  testScene2();
}

void
Game::exit(GameEngine*)
{
  delete mWaiting;
}

void
Game::save(StateSaver*)
{

}

void
Game::restore(StateSaver*)
{

}

/** Set a server to play 
  *
  * \param s the server
  *
  */
void
Game::setServer(Server* s)
{
  mServer = s;
}

/** Get the actual server
  *
  * The returned pointer may be NULL.
  *
  * \return The current server
  * 
  */
Server*
Game::getServer()
{
  return mServer;
}

void
Game::setMapSettings(NewMapSettings* nmap)
{
  mMapSettings = nmap;
}

void
Game::update(float elapsed)
{
  mWaiting->update(elapsed);
  Ogre::Vector3 p = mCamera->getPosition();
  
  mCamera->setPosition(p + (mDirection * elapsed));
  if (mMoving)
    logPosition();
}

void
Game::drawOverlay()
{
  mWaiting->draw();
}

void
Game::testScene()
{
  using namespace Ogre;
  
  //from http://www.ogre3d.org/forums/viewtopic.php?f=2&t=78388
  // Create the mesh via the MeshManager
    Ogre::MeshPtr msh = MeshManager::getSingleton().createManual("ColourCube", "General");
 
    /// Create one submesh
    SubMesh* sub = msh->createSubMesh();
 
    const float sqrt13 = 0.577350269f; /* sqrt(1/3) */
 
    /// Define the vertices (8 vertices, each consisting of 2 groups of 3 floats
    const size_t nVertices = 8;
    const size_t vbufCount = 3*2*nVertices;
    float vertices[vbufCount] = {
            -100.0,100.0,-100.0,        //0 position
            -sqrt13,sqrt13,-sqrt13,     //0 normal
            100.0,100.0,-100.0,         //1 position
            sqrt13,sqrt13,-sqrt13,      //1 normal
            100.0,-100.0,-100.0,        //2 position
            sqrt13,-sqrt13,-sqrt13,     //2 normal
            -100.0,-100.0,-100.0,       //3 position
            -sqrt13,-sqrt13,-sqrt13,    //3 normal
            -100.0,100.0,100.0,         //4 position
            -sqrt13,sqrt13,sqrt13,      //4 normal
            100.0,100.0,100.0,          //5 position
            sqrt13,sqrt13,sqrt13,       //5 normal
            100.0,-100.0,100.0,         //6 position
            sqrt13,-sqrt13,sqrt13,      //6 normal
            -100.0,-100.0,100.0,        //7 position
            -sqrt13,-sqrt13,sqrt13,     //7 normal
    };
 
    Ogre::RenderSystem* rs = Ogre::Root::getSingleton().getRenderSystem();
    RGBA colours[nVertices];
    RGBA *pColour = colours;
    // Use render system to convert colour value since colour packing varies
    rs->convertColourValue(ColourValue(1.0,0.0,0.0), pColour++); //0 colour
    rs->convertColourValue(ColourValue(1.0,1.0,0.0), pColour++); //1 colour
    rs->convertColourValue(ColourValue(0.0,1.0,0.0), pColour++); //2 colour
    rs->convertColourValue(ColourValue(0.0,0.0,0.0), pColour++); //3 colour
    rs->convertColourValue(ColourValue(1.0,0.0,1.0), pColour++); //4 colour
    rs->convertColourValue(ColourValue(1.0,1.0,1.0), pColour++); //5 colour
    rs->convertColourValue(ColourValue(0.0,1.0,1.0), pColour++); //6 colour
    rs->convertColourValue(ColourValue(0.0,0.0,1.0), pColour++); //7 colour
 
    /// Define 12 triangles (two triangles per cube face)
    /// The values in this table refer to vertices in the above table
    const size_t ibufCount = 36;
    unsigned short faces[ibufCount] = {
            0,2,3,
            0,1,2,
            1,6,2,
            1,5,6,
            4,6,5,
            4,7,6,
            0,7,4,
            0,3,7,
            0,5,1,
            0,4,5,
            2,7,3,
            2,6,7
    };
 
    /// Create vertex data structure for 8 vertices shared between submeshes
    msh->sharedVertexData = new Ogre::VertexData();
    msh->sharedVertexData->vertexCount = nVertices;
 
    /// Create declaration (memory format) of vertex data
    Ogre::VertexDeclaration* decl = msh->sharedVertexData->vertexDeclaration;
    size_t offset = 0;
    // 1st buffer
    decl->addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
    offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
    decl->addElement(0, offset, Ogre::VET_FLOAT3, VES_NORMAL);
    offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
    /// Allocate vertex buffer of the requested number of vertices (vertexCount) 
    /// and bytes per vertex (offset)
    Ogre::HardwareVertexBufferSharedPtr vbuf = 
      Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(
        offset, msh->sharedVertexData->vertexCount, Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);
    /// Upload the vertex data to the card
    vbuf->writeData(0, vbuf->getSizeInBytes(), vertices, true);
 
    /// Set vertex buffer binding so buffer 0 is bound to our vertex buffer
    Ogre::VertexBufferBinding* bind = msh->sharedVertexData->vertexBufferBinding; 
    bind->setBinding(0, vbuf);
 
    // 2nd buffer
    offset = 0;
    decl->addElement(1, offset, VET_COLOUR, VES_DIFFUSE);
    offset += Ogre::VertexElement::getTypeSize(VET_COLOUR);
    /// Allocate vertex buffer of the requested number of vertices (vertexCount) 
    /// and bytes per vertex (offset)
    vbuf = Ogre::HardwareBufferManager::getSingleton().createVertexBuffer(
        offset, msh->sharedVertexData->vertexCount, Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);
    /// Upload the vertex data to the card
    vbuf->writeData(0, vbuf->getSizeInBytes(), colours, true);
 
    /// Set vertex buffer binding so buffer 1 is bound to our colour buffer
    bind->setBinding(1, vbuf);
 
    /// Allocate index buffer of the requested number of vertices (ibufCount) 
    Ogre::HardwareIndexBufferSharedPtr ibuf = Ogre::HardwareBufferManager::getSingleton().
        createIndexBuffer(
			  Ogre::HardwareIndexBuffer::IT_16BIT, 
        ibufCount, 
			  Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY);
 
    /// Upload the index data to the card
    ibuf->writeData(0, ibuf->getSizeInBytes(), faces, true);
 
    /// Set parameters of the submesh
    sub->useSharedVertices = true;
    sub->indexData->indexBuffer = ibuf;
    sub->indexData->indexCount = ibufCount;
    sub->indexData->indexStart = 0;
 
    /// Set bounding information (for culling)
    msh->_setBounds(Ogre::AxisAlignedBox(-100,-100,-100,100,100,100));
    msh->_setBoundingSphereRadius(Ogre::Math::Sqrt(3*100*100));

    Ogre::SceneManager* sceneManager;
    try{
      sceneManager = Ogre::Root::getSingleton().getSceneManager("terrain");
    }
    catch (...){
      LOGE("Cannot get terrain scene manager");
      return;
    }

    /// Notify -Mesh object that it has been loaded
    msh->load();
    Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create(
									      "Test/ColourTest", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    material->getTechnique(0)->getPass(0)->setVertexColourTracking(Ogre::TVC_AMBIENT);
   Ogre::Entity* thisEntity = sceneManager->createEntity("cc", "ColourCube");
   thisEntity->setMaterialName("Test/ColourTest");
   Ogre::SceneNode* thisSceneNode = sceneManager->getRootSceneNode()->createChildSceneNode();
   thisSceneNode->setPosition(35, 0, 0);
   thisSceneNode->attachObject(thisEntity);

}

void
Game::testScene2()
{

Ogre::SceneManager* sceneManager = Ogre::Root::getSingleton().getSceneManager("terrain");
  
   // Another try from http://ogre3d.org/tikiwiki/tiki-index.php?page=MadMarx+Tutorial+4&structure=Tutorials
   Ogre::ManualObject* lManualObject = NULL;
   {
     
     // The manualObject creation requires a name.
     Ogre::String lManualObjectName = "CubeWithAxes";
     lManualObject = sceneManager->createManualObject(lManualObjectName);
     
     // Always tell if you want to update the 3D (vertex/index) later or not.
     bool lDoIWantToUpdateItLater = false;
     lManualObject->setDynamic(lDoIWantToUpdateItLater);
     
     // Here I create a cube in a first part with triangles, and then axes (in red/green/blue).
     
     // BaseWhiteNoLighting is the name of a material that already exist inside Ogre.
     // Ogre::RenderOperation::OT_TRIANGLE_LIST is a kind of primitive.
     float lSize = 0.7f;
     lManualObject->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);
     {
       float cp = 1.0f * lSize ;
       float cm = -1.0f * lSize;
       
       lManualObject->position(cm, cp, cm);// a vertex
       lManualObject->colour(Ogre::ColourValue(0.0f,1.0f,0.0f,1.0f));
       lManualObject->position(cp, cp, cm);// a vertex
       lManualObject->colour(Ogre::ColourValue(1.0f,1.0f,0.0f,1.0f));
       lManualObject->position(cp, cm, cm);// a vertex
       lManualObject->colour(Ogre::ColourValue(1.0f,0.0f,0.0f,1.0f));
       lManualObject->position(cm, cm, cm);// a vertex
       lManualObject->colour(Ogre::ColourValue(0.0f,0.0f,0.0f,1.0f));
       
       lManualObject->position(cm, cp, cp);// a vertex
       lManualObject->colour(Ogre::ColourValue(0.0f,1.0f,1.0f,1.0f));
       lManualObject->position(cp, cp, cp);// a vertex
       lManualObject->colour(Ogre::ColourValue(1.0f,1.0f,1.0f,1.0f));
       lManualObject->position(cp, cm, cp);// a vertex
       lManualObject->colour(Ogre::ColourValue(1.0f,0.0f,1.0f,1.0f));
       lManualObject->position(cm, cm, cp);// a vertex
       lManualObject->colour(Ogre::ColourValue(0.0f,0.0f,1.0f,1.0f));
       
       // face behind / front
       lManualObject->triangle(0,1,2);
       lManualObject->triangle(2,3,0);
       lManualObject->triangle(4,6,5);
       lManualObject->triangle(6,4,7);
       
       // face top / down
       lManualObject->triangle(0,4,5);
       lManualObject->triangle(5,1,0);
       lManualObject->triangle(2,6,7);
       lManualObject->triangle(7,3,2);
       
       // face left / right
       lManualObject->triangle(0,7,4);
       lManualObject->triangle(7,0,3);
       lManualObject->triangle(1,5,6);
       lManualObject->triangle(6,2,1);			
     }
     lManualObject->end();
     // Here I have finished my ManualObject construction.
     // It is possible to add more begin()-end() thing, in order to use 
     // different rendering operation types, or different materials in 1 ManualObject.
     lManualObject->begin("BaseWhiteNoLighting",Ogre::RenderOperation::OT_LINE_LIST);
     {
       float lAxeSize = 2.0f * lSize;
       lManualObject->position(0.0f, 0.0f, 0.0f);
       lManualObject->colour(Ogre::ColourValue::Red);
       lManualObject->position(lAxeSize, 0.0f, 0.0f);
       lManualObject->colour(Ogre::ColourValue::Red);
       lManualObject->position(0.0f, 0.0f, 0.0f);
       lManualObject->colour(Ogre::ColourValue::Green);
       lManualObject->position(0.0, lAxeSize, 0.0);
       lManualObject->colour(Ogre::ColourValue::Green);
       lManualObject->position(0.0f, 0.0f, 0.0f);
       lManualObject->colour(Ogre::ColourValue::Blue);
       lManualObject->position(0.0, 0.0, lAxeSize);
       lManualObject->colour(Ogre::ColourValue::Blue);
       
       lManualObject->index(0);
       lManualObject->index(1);
       lManualObject->index(2);
       lManualObject->index(3);
       lManualObject->index(4);
       lManualObject->index(5);
     }
     lManualObject->end();
   }
   Ogre::String lNameOfTheMesh = "MeshCubeAndAxe";
   Ogre::String lResourceGroup = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME;
   lManualObject->convertToMesh(lNameOfTheMesh);
   
   // Now I can create several entities using that mesh
   int lNumberOfEntities = 5;
   for(int iter = 0; iter < lNumberOfEntities; ++iter)
     {
       Ogre::Entity* lEntity = sceneManager->createEntity(lNameOfTheMesh);
       // Now I attach it to a scenenode, so that it becomes present in the scene.
       Ogre::SceneNode* lNode = sceneManager->getRootSceneNode()->createChildSceneNode();
       lNode->attachObject(lEntity);
       // I move the SceneNode so that it is visible to the camera.
       float lPositionOffset = float(1+ iter * 2) - (float(lNumberOfEntities));
       lNode->translate(lPositionOffset, lPositionOffset, -10.0f);
     }
}

bool
Game::mouseMoved(const OIS::MouseEvent& evt)
{
  mCamera->yaw(Ogre::Degree(-mRotate * evt.state.X.rel));
  mCamera->pitch(Ogre::Degree(-mRotate * evt.state.Y.rel));

  Ogre::Quaternion q = mCamera->getOrientation();
  float pitch = q.getPitch().valueDegrees();
  float yaw = q.getYaw().valueDegrees();

  // Get triangle count
  auto tr = mGameEngine->getRenderWindow()->getTriangleCount();
  
  LOGI("Moving mouse (pitch:" << pitch << ", yaw: " << yaw << ") Trianges:"
       << (int)tr);
  return true;
}

bool
Game::keyPressed( const OIS::KeyEvent&  evt)
{
  bool consumed = false;
  switch (evt.key)
    {
    case OIS::KC_UP:
    case OIS::KC_Z:
      mDirection.z = -mMove;
      consumed = true;
      mMoving = true;
      break;
      
    case OIS::KC_DOWN:
    case OIS::KC_S:
      mDirection.z = mMove;
      consumed = true;
      mMoving = true;
      break;
    
    case OIS::KC_LEFT:
    case OIS::KC_Q:
      mDirection.x = -mMove;
      consumed = true;
      mMoving = true;
      break;
    
    case OIS::KC_RIGHT:
    case OIS::KC_D:
      mDirection.x = mMove;
      consumed = true;
      mMoving = true;
      break;

    default:
      break;
    };

  return consumed;
}

void
Game::logPosition()
{
  Ogre::Vector3 p = mCamera->getPosition();
  LOGI("Cam pos. : x" <<  p.x << ", y" << p.y << ", y" << p.z);

}

bool
Game::keyReleased( const OIS::KeyEvent& evt)
{
  bool consumed = false;
  switch (evt.key)
    {
    case OIS::KC_UP:
    case OIS::KC_Z:
      mDirection.z = 0;
      consumed = true;
      mMoving = false;
      break;
      
    case OIS::KC_DOWN:
    case OIS::KC_S:
      mDirection.z = 0;
      consumed = true;
      mMoving = false;
      break;
    
    case OIS::KC_LEFT:
    case OIS::KC_Q:
      mDirection.x = 0;
      consumed = true;
      mMoving = false;
      break;
    
    case OIS::KC_RIGHT:
    case OIS::KC_D:
      mDirection.x = 0;
      consumed = true;
      mMoving = false;
      break;

    default:
      break;
    };

  return consumed;

}
