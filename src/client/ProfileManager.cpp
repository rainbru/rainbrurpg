/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ProfileManager.hpp"

#include "GameEngine.hpp"
#include "Logger.hpp"
#include "StateSaver.hpp"
#include "NewProfile.hpp"
#include "KeyList.hpp"
#include "RsaKey.hpp"

static Rpg::Logger static_logger("profile", Rpg::LT_BOTH);

ProfileManager::ProfileManager():
  GameState("ProfileManager"),
  mGameEngine(NULL),
  mMenuWindow(NULL),
  mNewProfile(NULL)
{

}

ProfileManager::~ProfileManager()
{

}

void
ProfileManager::enter(GameEngine* ge)
{
  // Keep a pointer to GameEngine to be able to go back to MainMenu
  mGameEngine = ge;

  try {
    mMenuWindow = loadLayout("profile_manager.layout", "ProfileManagerWin");
  }
  catch(CEGUI::UnknownObjectException& e)
    {
      LOGE("Error loading ProfileManager layout file");
    }

  // Handle events
  addEvent("ProfileManagerWin/btnBack",
	   CEGUI::PushButton::EventClicked,
	   CEGUI::Event::Subscriber(&ProfileManager::onBack, this));

  addEvent("ProfileManagerWin/btnNew",
	   CEGUI::PushButton::EventClicked,
	   CEGUI::Event::Subscriber(&ProfileManager::onNew, this));

  feedProfileList();
  addEvent("ProfileManagerWin/lbExisting",
	   CEGUI::PushButton::EventClicked,
	   CEGUI::Event::Subscriber(&ProfileManager::onChangeProfile, this));
  
}

void
ProfileManager::exit(GameEngine*)
{
  destroyRootWindow();

}

void
ProfileManager::save(StateSaver* st)
{
  LOGI("Saving ProfileManager state");
  // Scalars
  // st->set<string>("seed", getSeed());

  // Complex types
  st->save("profilemanager", mMenuWindow);
}

void
ProfileManager::restore(StateSaver* st)
{
  // Scalars
  //  setSeed(st->get<string>("seed"));
    
  // Complex types
  st->restore("profilemanager", mMenuWindow);

}

bool
ProfileManager::onBack(const CEGUI::EventArgs&)
{
  mGameEngine->toMainMenu();
  return true;
}

/** Opens a dialog used to create a new profile
  *
  *
  */
bool
ProfileManager::onNew(const CEGUI::EventArgs&)
{
  LOGI("Opening NewProfile dialog");
  if (mNewProfile)
    {
      mNewProfile->show();
    }
  else
    {
      mNewProfile = new NewProfile(this);
      mNewProfile->show();
    }
  
  return true;
}

void
ProfileManager::feedProfileList()
{
  KeyList kl;
  kl.load();

  auto lbWorlds = static_cast<Listbox*>
    (mMenuWindow->getChild("lbExisting"));
  lbWorlds->setMultiselectEnabled(false);
  lbWorlds->setSortingEnabled(true);

  
  list<string> keys = kl.getKeys();
  for (auto k : keys)
    {
      cout << "New key found : " << k << endl;

      // Try to know if the key has a password
      
      RsaKey rsa(kl.getKeyFile(k));
      if (rsa.hasPassword())
	k += " (*)";

      
      ListboxTextItem* lbi = new ListboxTextItem(k, 1);
      lbi->setSelectionColours(CEGUI::Colour(0.0f, 0.0f, 0.8f));
      lbi->setTextColours(CEGUI::Colour(0xFFFFFFFF));
      lbi->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
      lbWorlds->addItem(lbi);

    }
  
}

/** Called by NewProfile dialog when new profile is ready
 *
 */
void
ProfileManager::asyncUpdate()
{
  LOGI("In ProfileManager::asyncUpdate()");
  auto lbWorlds = static_cast<Listbox*>(mMenuWindow->getChild("lbExisting"));
  lbWorlds->resetList();
  feedProfileList();
}

/** The user double clicked on a profile, check password then change it
  * 
  */
bool
ProfileManager::onChangeProfile(const CEGUI::EventArgs&)
{
  LOGE("Changing profile");
  if (mNewProfile)
    {
      mNewProfile->show();
    }
  else
    {
      mNewProfile = new NewProfile(this);
      mNewProfile->show();
    }

}
