/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "NewProfile.hpp"

#include <CEGUI/CEGUI.h> // Uses CEGUI::PushButton
#include <CEGUI/WindowManager.h>

#include <sstream>

#include <stdlib.h>      /* srand */
#include <openssl/rsa.h>
#include <openssl/bn.h>  /* BIGNUM */
#include <openssl/bio.h> /* BIO* */
#include <openssl/pem.h>
#include <openssl/err.h> /* Uses ERR_error_string() */

#include "CeguiWindowList.hpp"
#include "Logger.hpp"
#include "RsaKey.hpp"
#include "KeyList.hpp"

#include <stdio.h>

using namespace std;

static Rpg::Logger static_logger("profile", Rpg::LT_BOTH);

/** Create a new profile dialog
 *
 * \param au The ProfileManager class to be upated when creation's ok.
 *
 */
NewProfile::NewProfile(AsyncUpdate* au):
  async(au),
  mDialogWindow(NULL),
  passphrase("")
{
  // Create dialog on first call
  LOGI("Creating NewProfile instance");
}

NewProfile::~NewProfile()
{
}

bool
NewProfile::onCancel(const CEGUI::EventArgs&)
{
  hide();
  return true;
}

/** An helper to get current EditBox value
  *
  * \param name The editBox name from the "newProfileRoot/winToolbar/"
  *        namespace.
  *
  */
std::string
NewProfile::getEbValue(const std::string& name)
{
  ostringstream oss;
  oss << "newProfileRoot/winToolbar/" << name;
  auto ebName = oss.str();
  CEGUI::Window* teSeed = mDialogWindow->getChild(ebName);
  return teSeed->getText().c_str();
}


bool
NewProfile::onCreate(const CEGUI::EventArgs&)
{
  // Get the EditBox values
  auto n = getEbValue("ebName");
  LOGI("Name =" <<n);

  auto pwd = getEbValue("ebPassword");
  auto conf = getEbValue("ebConfirm");

  // Check pwd length
  /*  if (pwd.size() < 8)
    {
      setErrorLabelVisible(true);
      setErrorMessage("Password must have at least 8 characters.");
      return true;
    }
  */
  if (pwd != conf)
    {
      setErrorLabelVisible(true);
      setErrorMessage("Confirmation doesn't match passord. Please check!");
    }
  else
    {
      KeyList kl;

      setErrorLabelVisible(false);
      // Create profile!
      passphrase = getEbValue("ebPassword");
      cout << "Password is '" << passphrase << "'" << endl;
      generateRsaKey(kl.getKeyFile(n));
      readRsaKey();

      // Add new profile to KeyList
      kl.load();
      if (!kl.checkKey(n))
	{
	  setErrorLabelVisible(true);
	  setErrorMessage(kl.getErrorString());
	}
      else
	{
	  kl.add(n);
	  hide();
	  async->asyncUpdate();
	}
    }

  return true;
}

void
NewProfile::show()
{
  if (mDialogWindow)
    {
    mDialogWindow->show();
    mDialogWindow->activate();
    
    LOGI("Showing mDialogWindow");
    
    }
  else
    create();
}

void
NewProfile::hide()
{
  if (mDialogWindow)
    mDialogWindow->hide();
}

void
NewProfile::create()
{
  auto mWmgr = CEGUI::WindowManager::getSingletonPtr();

  // Create the parent window using unique name, makes it fullsize
  // and transparent
  mDialogWindow=mWmgr->createWindow("Vanilla/StaticImage",
					 "newProfileWindow");
  
  mDialogWindow->setProperty("Size", "{{1,0},{1,0}}");
  mDialogWindow->setProperty("FrameEnabled", "False");
  mDialogWindow->setProperty("BackgroundEnabled", "False");
  
  // Load the given layout file
  CEGUI::Window* layoutWindow = mWmgr->loadLayoutFromFile("new_profile.layout");

  auto mRootWindow = CEGUI::System::getSingleton().getDefaultGUIContext().
    getRootWindow();
  mRootWindow->addChild(mDialogWindow);
  mDialogWindow->addChild(layoutWindow);

  // Handle events
  auto event = CEGUI::PushButton::EventClicked;
  auto cb_cancel = CEGUI::Event::Subscriber(&NewProfile::onCancel, this);
  auto btn_cancel = "newProfileWindow/newProfileRoot/winToolbar/btnCancel";
  mRootWindow->getChild(btn_cancel)->subscribeEvent(event, cb_cancel);

  auto btn_create = "newProfileWindow/newProfileRoot/winToolbar/btnCreate";
  auto cb_create = CEGUI::Event::Subscriber(&NewProfile::onCreate, this);
  mRootWindow->getChild(btn_create)->subscribeEvent(event, cb_create);

  // setTextMasked on EditBoxes
  auto strPwd = "newProfileWindow/newProfileRoot/winToolbar/ebPassword";
  auto ebPwd = dynamic_cast<CEGUI::Editbox*>(mRootWindow->getChild(strPwd));
  ebPwd->setTextMasked(true);

  auto strConf = "newProfileWindow/newProfileRoot/winToolbar/ebConfirm";
  auto ebConf = dynamic_cast<CEGUI::Editbox*>(mRootWindow->getChild(strConf));
  ebConf->setTextMasked(true);

  setErrorLabelVisible(false);
}

CEGUI::Window*
NewProfile::getErrorMessageLabel() const
{
  return mDialogWindow->getChild("newProfileRoot/winToolbar/ErrorMsg");
}


/** Change the actual error message
  *
  * Set the actual dialog error message with optional color.
  *
  * \param text  the new message.
  * \param color A color in the form FFFF0000.
  *
  */
void
NewProfile::setErrorMessage(const std::string& text,
			    const std::string& color)
{

  CEGUI::Window* err = getErrorMessageLabel();
  
  ostringstream oss;
  oss << "[colour='" << color << "']" << text;

  err->setText(oss.str());

}

/** Change the visibility status of the error label
  *
  * \param visible new state.
  *
  */
void
NewProfile::setErrorLabelVisible(bool visible)
{
  getErrorMessageLabel()->setVisible(visible);
}


/** Generate a new key pair
  *
  * \param path The full path to private key
  *
  */
void
NewProfile::generateRsaKey(const string& path)
{
  RsaKey rk(path);
  rk.write(passphrase);
}

/** Reading both public and private keys actually work
  *
  * But it can fail if generateRsaKey() is called before.
  *
  */
void
NewProfile::readRsaKey()
{

}
