/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _NEW_PROFILE_
#define _NEW_PROFILE_

#include <CEGUI/Window.h>
#include <CEGUI/EventArgs.h>

#include <string>

#include "AsyncUpdate.hpp"

/* A dialog used to create a new profile
 *
 * Note: this is not a CeguiDialog instance because the parenting is different.
 *
 */
class NewProfile
{
public:
  NewProfile(AsyncUpdate*);
  ~NewProfile();

  void show();
  void hide();

  void setErrorMessage(const std::string&,
		       const std::string& color = "FFFF0000");

  void setErrorLabelVisible(bool);
  
protected:
  void create();
  void generateRsaKey(const std::string&);
  void readRsaKey();
  
  bool onCancel(const CEGUI::EventArgs&);
  bool onCreate(const CEGUI::EventArgs&);

  std::string getEbValue(const std::string&);
  CEGUI::Window* getErrorMessageLabel() const;
  
private:
  AsyncUpdate* async;
  CEGUI::Window* mDialogWindow; /// The Dialog window
  std::string passphrase;       // The entered passphrase (if confirmed)
};

#endif // !_NEW_PROFILE_
