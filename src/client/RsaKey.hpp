/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _RSA_KEY_HPP_
#define _RSA_KEY_HPP_

#include <string>
#include <openssl/rsa.h> /* Uses RSA* */

using namespace std;

/** A class used to read/write RSA keys to/from disk using PEM format
  *
  */
class RsaKey
{
public:
  RsaKey(const string&);
  ~RsaKey();

  int write(string&);
  RSA* read(string&);

  const string& getPrivateKeyName() const;
  const string& getPublicKeyName() const;

  bool hasPassword();
  
protected:
  int writePublicKey();
  int writePrivateKey(string&);

  int readPublicKey();
  RSA* readPrivateKey(string&);
  
private:
  /** The keys will be named after basename
    *
    * <basename>     private key
    * <basename>.pub public key
    *
    * Note: we're using a placeholder password when the user doesn't enter
    * one because we can't save a private with no password (empty file).
    */
  string privateKeyName; 
  string publicKeyName;
  RSA* rsaPrivateKey;

  /** A password used to write private key when user didn't
    * enter a password
    *
    */
  string placeholder_pwd;

};

#endif // !_RSA_KEY_HPP_
