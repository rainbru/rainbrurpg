/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _KEY_LIST_HPP_
#define _KEY_LIST_HPP_

#include <map>
#include <list>
#include <string>

using namespace std;

/** Handle a persistant RsaKey list
  *
  * This class is used to retrieve a RsaKey basename (with the '.pub' extension
  * of the public key, from the username.
  *
  */
class KeyList
{
public:
  KeyList();
  ~KeyList();

  const string& getKeylist() const;
  const string& getKeydir() const;
  const string& getErrorString() const;
  string getKeyFile(const string&);

  void add(const string&);
  size_t mapSize()const;

  bool checkKey(const string&);
  void save();
  void load();
  
  void remove_keylist();

  list<string> getKeys();
  
protected:
  string keylist;  //!< Will contain a CSV list of sanitized username/key
  string keydir;   //!< Will contain individual keys (public and private);
  map<string, string> filemap; //!< The mapping of the file

  string error_string; //!< Set when checkKey failed
};

#endif  // !_KEY_LIST_HPP_
