/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _SCENE_HANDLER_HPP_
#define _SCENE_HANDLER_HPP_

// Forward declaration
namespace Ogre
{
  class Viewport;
}  
// End of forward declaration


/** Defines the currently used SceneManager type
  *
  */
enum SceneHandlerType{
  UNDEFINED = 0, // Not yet defined
  GENERIC,   // A generic scene used for HUD and menus
  TERRAIN    // The terrain drawing scene
};

/** The main scene manager changing class
  *
  * An instance of this class is used by GameEngine to change Ogre3D scene 
  * manager, because the client need regular scene manager changes between 
  * a generic SceneManager and a Terrain-oriented one.
  *
  */
class SceneHandler
{
public:
  SceneHandler();
  ~SceneHandler();

  void createGeneric();
  void createTerrain();

  void toGeneric();
  void toTerrain();
  
public:
  SceneHandlerType mSceneType;
  bool terrainInitialised;
  Ogre::Viewport* mViewport;
};

#endif // _SCENE_HANDLER_HPP_
