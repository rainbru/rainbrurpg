/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _PROFILE_MANAGER_HPP_
#define _PROFILE_MANAGER_HPP_

#include "GameState.hpp"
#include "AsyncUpdate.hpp"

// Forward declaration
class GameEngine;
class StateSaver;
class NewProfile;
namespace CEGUI
{
  class Window;
}
// End of forward declaration


class ProfileManager: public GameState, public AsyncUpdate
{
public:
  ProfileManager();
  virtual ~ProfileManager();

  void enter(GameEngine*);
  void exit(GameEngine*);
  void save(StateSaver*);
  void restore(StateSaver*);
  void feedProfileList();

  virtual void asyncUpdate();
  
protected:
  bool onBack(const CEGUI::EventArgs&);
  bool onNew(const CEGUI::EventArgs&);
  bool onChangeProfile(const CEGUI::EventArgs&);
  
private:
  GameEngine* mGameEngine;    //!< Keep a pointer to the GameEngine
  NewProfile* mNewProfile;
  CEGUI::Window* mMenuWindow; //!< The main layout window
};

#endif // !_PROFILE_MANAGER_HPP_
