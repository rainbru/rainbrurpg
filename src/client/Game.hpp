/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _GAME_HPP_
#define _GAME_HPP_

#include <OgreVector3.h>

#include "GameState.hpp"

#include "LocalTest.hpp" // Uses NewMapSettings

// Formard declarations
class Server;
class WaitingCircle;
class GameEngine;
namespace Ogre
{
  class Camera;
}
// End of formard declarations

/** The game state
  *
  * Player is entering a world and starts playing.
  *
  */
class Game: public GameState
{
public:
  Game();
  virtual ~Game();
  
  virtual void enter(GameEngine*);
  virtual void exit(GameEngine*);

  virtual void save(StateSaver*);
  virtual void restore(StateSaver*);

  virtual void update(float);
  virtual void drawOverlay();

  virtual bool mouseMoved(const OIS::MouseEvent&);
  virtual bool keyPressed( const OIS::KeyEvent& );
  virtual bool keyReleased( const OIS::KeyEvent& );
  
  void setServer(Server*);
  Server* getServer();

  void setMapSettings(NewMapSettings*);

 
protected:
  void testScene();
  void testScene2();

  void logPosition();
  
private:
  Server* mServer;
  NewMapSettings* mMapSettings;
  WaitingCircle* mWaiting;
  Ogre::Camera* mCamera;
  GameEngine* mGameEngine;
  
  float mRotate;
  float mMove;
  Ogre::Vector3 mDirection;
  bool mMoving;   // Is the player moving ?
};


#endif // _GAME_HPP_
