/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "SceneHandler.hpp"

#include <Logger.hpp>
#include <Ogre.h>

using namespace Ogre;

static Rpg::Logger static_logger("scenehandler", Rpg::LT_BOTH);


SceneHandler::SceneHandler():
  mSceneType(UNDEFINED),
  terrainInitialised(false),
  mViewport(NULL)
{

}

SceneHandler::~SceneHandler()
{

}

/** Create and set a generic SceneManager
  *
  * We can't call this directly from constructor, because this class is used
  * by GameEngine and Ogre::Root is only created in 
  * GameEngine::initialiseOgre().
  *
  */
void SceneHandler::createGeneric()
{
  mSceneType=GENERIC;

  Ogre::Root* mRoot= Ogre::Root::getSingletonPtr();
  Ogre::RenderWindow* mWindow = mRoot->getAutoCreatedWindow();
  
  // Create the SceneManager, in this case a generic one
  Ogre::SceneManager* mSceneMgr = mRoot->createSceneManager(ST_GENERIC, "generic");

  // Create the camera
  Camera* mCamera = mSceneMgr->createCamera("MenuCam");
      
  // Create one viewport, entire window
  mViewport = mWindow->addViewport(mCamera);
  //  mViewport->setBackgroundColour(Ogre::ColourValue(0,0,0));

  // Alter the camera aspect ratio to match the viewport
  mCamera->setAspectRatio(Ogre::Real(mViewport->getActualWidth()) /
			  Ogre::Real(mViewport->getActualHeight()));

  // Set ambient light
  //  mSceneMgr->setAmbientLight(Ogre::ColourValue(0.0, 0.0, 0.0));
mSceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));
  
  // Create a light
  Ogre::Light* l = mSceneMgr->createLight("MainLight");
  l->setPosition(20,80,50);

}

void SceneHandler::createTerrain()
{
  terrainInitialised = true;

  Ogre::Root* mRoot= Ogre::Root::getSingletonPtr();
  Ogre::RenderWindow* mWindow = mRoot->getAutoCreatedWindow();
  
  // Create the SceneManager, in this case a generic one
  Ogre::SceneManager* mSceneMgr = mRoot->createSceneManager(ST_GENERIC, "terrain");

  // Create the camera
  Camera* mCamera = mSceneMgr->createCamera("PlayerCam");

  bool infiniteClip =
    mRoot->getRenderSystem()->getCapabilities()
    ->hasCapability(Ogre::RSC_INFINITE_FAR_PLANE);
  
  if (infiniteClip)
    mCamera->setFarClipDistance(0);
  else
    mCamera->setFarClipDistance(50000);
  
  // Create one viewport, entire window
  //  Ogre::Viewport* vp = mWindow->addViewport(mCamera);
  //  vp->setBackgroundColour(Ogre::ColourValue(0,0,0));
  mViewport->setCamera(mCamera);
  mViewport->setBackgroundColour(Ogre::ColourValue(1,0,1));
  
  // Alter the camera aspect ratio to match the viewport
  mCamera->setAspectRatio(Ogre::Real(mViewport->getActualWidth()) /
			  Ogre::Real(mViewport->getActualHeight()));

  // Set ambient light
  mSceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));

  // Create a light
  Ogre::Light* l = mSceneMgr->createLight("MainLight");
  l->setPosition(20,80,50);

}

void
SceneHandler::toGeneric()
{
  if (mSceneType == GENERIC)
    return;
      
  // Need to change
}

void
SceneHandler::toTerrain()
{
  if (!terrainInitialised)
    createTerrain();

  if (mSceneType == TERRAIN)
    return;

  // Need to change
}
