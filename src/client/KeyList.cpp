/*
 * Copyright 2011-2018 Jerome Pasquier
 *
 * This file is part of rainbrurpg-client.
 *
 * rainbrurpg-client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurpg-client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurpg-client.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "KeyList.hpp"

#include <FileSystem.hpp>

#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>

#include <iostream>

#include "Logger.hpp"

using namespace RLGL;
using namespace std;

namespace fs = boost::filesystem;

static Rpg::Logger static_logger("keylist", Rpg::LT_BOTH);

KeyList::KeyList()
{
  keylist = FileSystem::join(FileSystem::getUserDir(".rainbrurpg/"), "keylist");
  keydir = FileSystem::join(FileSystem::getUserDir(".rainbrurpg/"), "keys/");
  FileSystem::createPathIfNeeded(keydir);
}

KeyList::~KeyList()
{

}

const string&
KeyList::getKeylist() const
{
  return keylist;
}

const string&
KeyList::getKeydir() const
{
  return keydir;
}

size_t
KeyList::mapSize()const
{
  return filemap.size();
}


void
KeyList::add(const string& username)
{
  filemap[username] = keydir + FileSystem::sanitize(username);
  save();
}

string
KeyList::getKeyFile(const string& username)
{
  auto val = FileSystem::join(keydir, username);
  return val;
}

void
KeyList::save()
{
  cout << "Saving in '" << keylist << "'" << endl;

  ofstream f;
  f.open (keylist);

  for(auto const &it : filemap)
    f << '"' << it.first << '"' << ',' << it.second << endl;

  f.close();
}

/** Remove the keylist file
 *
 */
void
KeyList::remove_keylist()
{
  fs::path p(keylist);
  fs::remove_all(p);
}

/** Load the keylist file
 *
 */
void
KeyList::load()
{

  LOGI("Loading keylist from" << keylist);

  string line, k1, k2;
  ifstream f;
  std::string delimiter = ",";

  // Doesn't block client if keylist doesn't exist
  if (!fs::exists(keylist))
    {
      LOGW("KeyList doesn't exist!!");
      return;
    }
  
  f.open(keylist);
  while(!f.eof()){
      
      //read data from file
      line.clear();
      getline (f, line);

      auto pos = line.find(delimiter);
      if (pos != std::string::npos)
	{
	  k1 = line.substr(0, pos);
	  k1.erase( 0, 1 ); // erase the first character
	  k1.erase( k1.size() - 1 ); // erase the last character
	  LOGI( "k1 shouldn't1 have \" :" << k1);
	  k2 = line.substr(pos);
	  filemap[k1] = k2;
	  LOGI("Adding " << k1 << " => " << k2 << " to filemap");
	}
    }

  f.close();

}

const string&
KeyList::getErrorString() const
{
  return error_string;
}


/** Check that the key doesn't contain illegal character (,)
 *
 * \return false if check failed
 *
 */
bool
KeyList::checkKey(const string& key)
{
  auto pos = key.find(",");
  if (pos == std::string::npos)
    return true;
  else
    error_string = "Username can't contain comma";
    return false;
}

list<string>
KeyList::getKeys()
{
  list<string> keys;
  
  for (std::map<string,string>::iterator it=filemap.begin();
       it!=filemap.end(); ++it)
    keys.push_back(it->first);

  return keys;
}

