#!/bin/sh -e  

# Ogre is in universe
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) main universe restricted multiverse"
# For clang-3.8
sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu/ trusty-updates main universe"

sudo apt-get update -qq

# Here I use --force-yes due to llvm signature errors
sudo apt-get --yes install libogre-1.9-dev libois-dev mercurial \
     libboost-program-options-dev libboost-system-dev libboost-filesystem-dev \
     libboost-graph-dev libboost-test-dev libboost-regex-dev \
     libenet-dev guile-2.0-dev libgit2-dev libssh2-1-dev\
     clang clang-3.4 libgtest-dev google-mock  \
     gcovr curl libfreeimage-dev cmake

# Install CEGUI-0 and CEGUI-0-OGRE
hg clone https://bitbucket.org/cegui/cegui cegui-source
cd cegui-source/
hg update -C v0-8
mkdir build
##  Apply gist patch : as of 2018-04-20, this patch is not needed anymore!
#  git clone https://gist.github.com/2a0fb8205b02871ebd2065822a9ebf48.git
#  hg patch --no-commit 2a0fb8205b02871ebd2065822a9ebf48/cegui.patch
## Continue building
cd build/
cmake ..
make
sudo make install

# Manually build lcov 1.11
cd ${TRAVIS_BUILD_DIR}
wget http://downloads.sourceforge.net/ltp/lcov-1.11.tar.gz
tar xvfz lcov-1.11.tar.gz
cd lcov-1.11
sudo make install

## Handle linking from /usr/local
echo "/usr/local/lib" | sudo tee /etc/ld.so.conf.d/local-lib.conf
sudo ldconfig

# build libgtest & libgtest_main
sudo mkdir /usr/src/gtest/build
sudo chmod 777 /usr/src/gtest/build
cd /usr/src/gtest/build
cmake .. -DBUILD_SHARED_LIBS=1
make -j4
sudo ln -s /usr/src/gtest/build/libgtest.so /usr/lib/libgtest.so
sudo ln -s /usr/src/gtest/build/libgtest_main.so /usr/lib/libgtest_main.so
