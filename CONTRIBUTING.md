# How to contribute

I'm really glad you're reading this, because we need volunteer developers to help this project come to fruition. We want you working on things you're excited about.

Here are some important resources:

  * [Our roadmap](https://github.com/rainbru/rainbrurpg/blob/master/ROADMAP) is the main project road map.
  * [Official website](http://rainbrurpg.org) the landing page of the project.
  * [Devlog](https://dailycommit.blogspot.com) the development blog.

## Testing

We have a handful of gtest features. Please write unit tests for new code 
you create.

## Submitting changes

Please send a [GitHub Pull Request to rainbrurpg](https://github.com/rainbru/rainbrurpg/pull/new/master) with a clear list of what you've done (read more about [pull requests](https://help.github.com/articles/about-pull-requests/)). When you send a pull request, we will love you forever if you include unit tests. We can always use more test coverage. Please follow our coding conventions (below) and make sure all of your commits are atomic (one feature per commit).

Always write a clear log message for your commits. One-line messages are fine for small changes, but bigger changes should look like this:

    $ git commit -m "A brief summary of the commit
    > 
    > A paragraph describing what changed and its impact."

## Coding conventions

Start reading our code and you'll get the hang of it. We optimize for readability:

  * We indent using two spaces (soft tabs)
  * We ALWAYS put spaces after list items and method parameters (`[1, 2, 3]`, not `[1,2,3]`) and around operators (`x += 1`, not `x+=1`).
  * This is open source software. Consider the people who will read your code, and make it look nice for them. It's sort of like driving a car: Perhaps you love doing donuts when you're alone, but with passengers the goal is to make the ride as smooth as possible.

This page was greatly inspired by [OpenGovernment's one](https://github.com/opengovernment/opengovernment/blob/master/CONTRIBUTING.md).

Thanks,
Jérôme Pasquier
